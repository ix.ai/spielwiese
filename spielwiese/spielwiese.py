""" Siple Web server that gives information about the request """

import logging
import os
import platform
import socket
import psutil
from flask import Flask
from flask import request
from flask import render_template
from flask import jsonify
from waitress import serve
from .lib import constants

log = logging.getLogger('spielwiese')
spielwiese = Flask(__package__, template_folder='templates')
version = f'{constants.VERSION}-{constants.BUILD}'


@spielwiese.route('/<path:path>', methods=['GET', 'POST', 'DELETE', 'PUT'])
@spielwiese.route('/', methods=['GET', 'POST', 'DELETE', 'PUT'])
def service(path="/"):
    """ Handles the reqeusts """
    log.debug(f"Serving request for path {path}")
    best_response = request.accept_mimetypes.best_match(['text/plain', 'text/html', 'application/json'])
    log.debug(f"Accepted mimetypes: {request.accept_mimetypes}")
    response = text_service()
    # Try to figure out which output to serve
    try:
        if 'json' in best_response:
            response = json_service()
        if 'html' in best_response:
            response = html_service()
    except TypeError:
        log.debug(f"Can't find a better matching accepted mime type: {request.accept_mimetypes}")
    return response


@spielwiese.route('/html', methods=['GET', 'POST', 'DELETE', 'PUT'])
def html_service():
    """ Returns an HTML response of class flask.app.response """
    log.debug("Serving HTML response")
    return spielwiese.response_class(
        response=f"{render_template('spielwiese.html.j2',version=version,data=collect_data())}\n",
        status=200
    )


@spielwiese.route('/json', methods=['GET', 'POST', 'DELETE', 'PUT'])
def json_service():
    """ Returns a JSON response of class flask.jsonify """
    log.debug("Serving JSON response")
    return jsonify({k: v for i in collect_data() for k, v in i.items()})


@spielwiese.route('/text', methods=['GET', 'POST', 'DELETE', 'PUT'])
def text_service():
    """ Returns a text-only response of class flask.app.response """
    log.debug("Serving TEXT response")
    return spielwiese.response_class(
        response=render_template(
            'spielwiese.txt.j2',
            version=version,
            data=collect_data(),
            mimetype="text/plain",
        ),
        status=200
    )


@spielwiese.route('/bench', methods=['GET', 'POST', 'DELETE', 'PUT'])
def benchmark():
    """ Always returns a text-only response of class flask.app.response with the content `1` """
    return spielwiese.response_class(response="1", status=200, mimetype="text/plain")


def collect_data():
    """ Returns basic information """

    data = [
        {'request': f"{request.method} {request.path} {request.environ.get('SERVER_PROTOCOL')}"},
        {'hostname': platform.node()},
        {'uname': os.uname().version},
        {'ram': str(round(psutil.virtual_memory().total / (1024.0 ** 3)))+" GB"},
        {'remote_addr': f"[{request.environ.get('REMOTE_ADDR')}]:{request.environ.get('REMOTE_PORT')}"},
    ]
    for netif, details in psutil.net_if_addrs().items():
        for detail in details:
            if detail.family is socket.AF_INET or detail.family is socket.AF_INET6:
                data.append({f"{netif}": detail.address})

    for header, value in request.headers:
        data.append({header: value})
    return data


def start():
    """ Starts spielwiese """
    port = int(os.environ.get('PORT', 8000))
    host = os.environ.get('ADDRESS', '*')

    log.info(f"Starting {__package__} {version}. Listening on {host}:{port}")
    serve(
        spielwiese,
        host=host,
        port=port,
        ident=f'{__package__} {version}'
    )


if __name__ == '__main__':
    start()
