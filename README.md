# spielwiese

[![Pipeline Status](https://gitlab.com/ix.ai/spielwiese/badges/master/pipeline.svg)](https://gitlab.com/ix.ai/spielwiese/)
[![Docker Stars](https://img.shields.io/docker/stars/ixdotai/spielwiese.svg)](https://hub.docker.com/r/ixdotai/spielwiese/)
[![Docker Pulls](https://img.shields.io/docker/pulls/ixdotai/spielwiese.svg)](https://hub.docker.com/r/ixdotai/spielwiese/)
[![Docker Image Version (latest)](https://img.shields.io/docker/v/ixdotai/spielwiese/latest)](https://hub.docker.com/r/ixdotai/spielwiese/)
[![Docker Image Size (latest)](https://img.shields.io/docker/image-size/ixdotai/spielwiese/latest)](https://hub.docker.com/r/ixdotai/spielwiese/)
[![Gitlab Project](https://img.shields.io/badge/GitLab-Project-554488.svg)](https://gitlab.com/ix.ai/spielwiese/)

Tiny python webserver that prints HTTP request information

## Usage

* supported methods: `GET`, `POST`, `DELETE`, `PUT`
* detects automatically if the client supports `html` or if the request has been made with the content type`application/json`

### Paths

* `/`: normal request with auto detection about the content type
* `/html`: normal request with minimal information about the request headers - force `html` output
* `/json`: normal request with minimal information about the request headers - force `json` output
* `/text`: normal request with minimal information about the request headers - force `text` output
* `/bench`: always returns the same output (`1`)

**Note**: Use `Content-Type: application/json` to get `json` output and use a browser that supports `html` mime type to have a little formatting in the output.

## Demo and running `ixdotai/spielwiese`

Try it out on [spielwiese.ix.ai](https://spielwiese.ix.ai)

```
[tlex@tmba]-[ ~ ]$ curl https://spielwiese.ix.ai
request: GET / HTTP/1.1
hostname: e3b776b99fba
uname: #32~18.04.1-Ubuntu SMP Mon Feb 3 14:05:59 UTC 2020
ram: 63 GB
remote_addr: [10.0.13.87]:34698
lo: 127.0.0.1
eth0: 10.0.13.107
eth1: 172.18.0.15
Host: spielwiese.ix.ai
User-Agent: curl/7.68.0
Accept: */*
Accept-Encoding: gzip
Cdn-Loop: cloudflare
Cf-Connecting-Ip: 2001:db8::1
Cf-Ipcountry: DE
Cf-Pseudo-Ipv4: 192.0.2.1
Cf-Ray: 56de12694a3cd467-HAM
Cf-Visitor: {"scheme":"https"}
X-Forwarded-For: 192.0.2.2
X-Forwarded-Host: spielwiese.ix.ai
X-Forwarded-Port: 443
X-Forwarded-Proto: https
X-Forwarded-Server: 4de60fc110d7
X-Real-Ip: 192.0.2.2
```

Or run it yourself:

```sh
docker run --rm -p 8000:8000 ixdotai/spielwiese
```

Or deploy it with `docker-compose`:

```yml
version: "3.7"

services:
  spielwiese:
  image: ixdotai/spielwiese:latest
  ports:
    - '8000:8000'
```

### Envionment Variables

| **Variable**        | **Default**      | **Description**                                                        |
|:--------------------|:----------------:|:-----------------------------------------------------------------------|
| `ADDRESS`           | `*`              | the address for spielwiese to listen on |
| `LOGLEVEL`          | `INFO`           | [Logging Level](https://docs.python.org/3/library/logging.html#levels) |
| `PORT`              | `8000`           | the port for incoming connections |

### Templates

The Jinja2 templates for the `html` and `text` outputs can be found in [spielwiese/templates/](https://gitlab.com/ix.ai/spielwiese/-/tree/master/spielwiese/templates).

## Resources:
* GitLab: https://gitlab.com/ix.ai/spielwiese
* Docker Hub: https://hub.docker.com/r/ixdotai/spielwiese
