FROM public.ecr.aws/docker/library/alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c AS builder

COPY spielwiese/requirements.txt /work/spielwiese/requirements.txt

ENV CRYPTOGRAPHY_DONT_BUILD_RUST="1"

RUN set -xeu; \
    mkdir -p /work/wheels; \
    apk add \
      py3-pip \
      gcc \
      python3-dev \
      musl-dev \
      linux-headers \
    ; \
    pip3 install -U --break-system-packages \
      wheel \
      pip

RUN pip3 wheel --prefer-binary -r /work/spielwiese/requirements.txt -w /work/wheels

FROM public.ecr.aws/docker/library/alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c
LABEL maintainer="docker@ix.ai" \
      ai.ix.repository="ix.ai/spielwiese" \
      org.opencontainers.image.source="https://gitlab.com/ix.ai/spielwiese"

COPY --from=builder /work /

RUN set -xeu; \
    apk --no-cache upgrade; \
    apk --no-cache add \
        python3 \
        py3-pip \
    ; \
    pip3 install \
        --no-index \
        --no-cache-dir \
        --find-links /wheels \
        --break-system-packages \
        -r /spielwiese/requirements.txt \
    ; \
    rm -rf /wheels

COPY spielwiese/ /spielwiese
COPY spielwiese.sh /usr/local/bin/spielwiese.sh

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/spielwiese.sh"]
